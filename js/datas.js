let button;
let permissionGranted = false;
let ios13 = false;
let android = false;

// request access to sensor on IOS13
function requestAccess() {
  DeviceOrientationEvent.requestPermission()
    .then((response) => {
      if (response == "granted") {
        permissionGranted = true;
      }
    })
    .catch(console.error);

  button.remove();
}
// access to sensor on andoid or non IOS13
function handleOrientation(event) {
  var absolute = event.absolute;
  var alpha = event.alpha;
  var rotationX = event.beta;
  var rotationY = event.gamma;
}
function detectDevice() {
  if (
    // ios 13 device
    typeof DeviceOrientationEvent !== "undefined" &&
    typeof DeviceOrientationEvent.requestPermission === "function"
  ) {
    ios13 = true;

    button = createButton("CLICK TO START");
    button.style("font-size", "2rem");
    button.style("border-radius", "50px");
    button.style("width", windowWidth-20 + "px");
    button.style("padding", "3rem");
    button.style("border", "15pt ridge pink");
    button.style("background-color", "green");
    button.style("color", "lightgreen");
    

    button.center();

    button.mousePressed(requestAccess);
  } else {
    // non IOS13 (android)
    others = true;
    // Way to do this in 2019 +
    window.addEventListener("deviceorientation", handleOrientation, true);
  }
}

let accelThreshold = 30;
let movThreshold = 1.5;
function setup() {
  detectDevice();
  angleMode(DEGREES);
  setShakeThreshold(accelThreshold);
  setMoveThreshold(movThreshold);
}
function draw() {
  // ROATION
  document.getElementById("rotZ").innerHTML = rotationZ.toFixed(3) + "°";
  document.getElementById("rotX").innerHTML = rotationX.toFixed(3) + "°";
  document.getElementById("rotY").innerHTML = rotationY.toFixed(3) + "°";
  // ACCELERATION
  document.getElementById("accelZ").innerHTML = accelerationZ.toFixed(3) + "m/s";
  document.getElementById("accelX").innerHTML = accelerationX.toFixed(3) + "m/s";
  document.getElementById("accelY").innerHTML = accelerationY.toFixed(3) + "m/s";
}
function deviceShaken() { // si l'accel X et Y depasse le threshold
  document.getElementById("shaken").innerHTML = "SECOUÉ !";
  setTimeout(() => {
    document.getElementById("shaken").innerHTML = "";
}, 2000);
}
function deviceMoved() {
  document.getElementById("moved").innerHTML = "BOUGÉ !";
  setTimeout(() => {
    document.getElementById("moved").innerHTML = "";
}, 2000);
}
function deviceTurned() {
  if (turnAxis === 'X') {
    document.getElementById("turnX").innerHTML = "Axe X tourné";
    setTimeout(() => {
      document.getElementById("turnX").innerHTML = "";
    }, 2000);
  }
  else if (turnAxis === 'Y') {
    document.getElementById("turnY").innerHTML = "Axe Y tourné";
    setTimeout(() => {
      document.getElementById("turnY").innerHTML = "";
    }, 2000);
  }
  else if (turnAxis === 'Z') {
    document.getElementById("turnZ").innerHTML = "Axe Z tourné";
    setTimeout(() => {
      document.getElementById("turnZ").innerHTML = "";
    }, 2000);
  }
}