
let button;
let permissionGranted = false;
let cx, cy;

let ios13 = false;
let others = false;

let r, g, b;

function setup(){
    createCanvas(windowWidth, windowHeight);
    randomColor();
    
    cx = width/2;
    cy = height/2;

// si IOS 13 device
    if (typeof(DeviceOrientationEvent) !== 'undefined' && typeof(DeviceOrientationEvent.requestPermission) === 'function'){
        // ios 13 device
        ios13 = true;
        background(0,255,0); // vert

        button = createButton("Click to allow access to sensors");
        button.style("font-size", "25px");
        button.center();

        button.mousePressed( requestAccess );
    } else {
        // non ios13 device 
        others = true;
       // Way to do this in 2019 +
       window.addEventListener("deviceorientation", handleOrientation, true);
    }
}// fin du setup

// function pour demander l'acces aux capteurs sr IOS 13
function requestAccess(){
    DeviceOrientationEvent.requestPermission()
        .then(response => {
            if(response == 'granted'){
                permissionGranted = true;
            }
        })
    .catch(console.error);

    button.remove();
} // fin de la fonction request access

// function pour gérer les data des others devices
function handleOrientation(event) {
   var absolute = event.absolute;
   var alpha    = event.alpha;
   var rotationX     = event.beta;
   var rotationY    = event.gamma;
 }

function draw(){
    // si on a affaire a un IOS13
    if(ios13){
    // si la permission n'est pas autorisé, on retourne au debut de boucle
     if(!permissionGranted) return;

    //rotationX, rotationY
    const dx = constrain(rotationY, -3, 3);
    const dy = constrain(rotationX, -3, 3);
    cx += dx*2;
    cy += dy*2;

    cx = constrain(cx, 0, width);
    cy = constrain(cy, 0, height);

    ellipse(cx, cy, 100, 100);
    //line(cx, cy, pRotationX, pRotationY);
    fill(r, g, b);
   } // fin IOS 13


   else if (others){
       //rotationX, rotationY
    const dx = constrain(rotationY, -3, 3);
    const dy = constrain(rotationX, -3, 3);
    cx += dx*2;
    cy += dy*2;

    cx = constrain(cx, 0, width);
    cy = constrain(cy, 0, height);

    ellipse(cx, cy, 100, 100);
    //line(cx, cy, pRotationX, pRotationY);
    fill(r, g, b);
   } // fin others

   else {
       textSize(45);
       text("error", 100, 100);
   }

} // fin du draw


function mousePressed(r, g, b) {
  randomColor();
 }

 function randomColor(){
   r = random(0, 255);
   g = random(0, 255);
   b = random(0, 255);
   return r, g, b;
 }