let button;
let permissionGranted = false;
let ios13 = false;
let android = false;

// request access to sensor on IOS13
function requestAccess() {
  DeviceOrientationEvent.requestPermission()
    .then((response) => {
      if (response == "granted") {
        permissionGranted = true;
      }
    })
    .catch(console.error);

  button.remove();
}
// access to sensor on andoid or non IOS13
function handleOrientation(event) {
  var absolute = event.absolute;
  var alpha = event.alpha;
  var rotationX = event.beta;
  var rotationY = event.gamma;
}
function detectDevice() {
  if (
    // ios 13 device
    typeof DeviceOrientationEvent !== "undefined" &&
    typeof DeviceOrientationEvent.requestPermission === "function"
  ) {
    ios13 = true;
    background(0, 255, 0); // vert

    button = createButton("CLICK TO START");
    button.style("font-size", "2rem");
    button.style("border-radius", "50px");
    button.style("width", width-20 + "px");
    button.style("padding", "3rem");
    button.style("border", "15pt ridge pink");
    button.style("background-color", "green");
    button.style("color", "lightgreen");
    

    button.center();

    button.mousePressed(requestAccess);
  } else {
    // non IOS13 (android)
    others = true;
    // Way to do this in 2019 +
    window.addEventListener("deviceorientation", handleOrientation, true);
  }
}
function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  detectDevice();
}
function draw() {
  background(200);
  //rotateZ(radians(rotationZ));
  rotateX(radians(rotationX));
  rotateY(radians(rotationY));
  box(200, 200, 200);
  describe(`red horizontal line right, green vertical line bottom.
      black background.`);
}
