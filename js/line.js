let button;
let permissionGranted = false;
let ios13 = false;
let android = false;

// request access to sensor on IOS13
function requestAccess() {
  DeviceOrientationEvent.requestPermission()
    .then((response) => {
      if (response == "granted") {
        permissionGranted = true;
      }
    })
    .catch(console.error);

  button.remove();
}
// access to sensor on andoid or non IOS13
function handleOrientation(event) {
  var absolute = event.absolute;
  var alpha = event.alpha;
  var rotationX = event.beta;
  var rotationY = event.gamma;
}
function detectDevice() {
  if (
    // ios 13 device
    typeof DeviceOrientationEvent !== "undefined" &&
    typeof DeviceOrientationEvent.requestPermission === "function"
  ) {
    ios13 = true;

    button = createButton("CLICK TO START");
    button.style("font-size", "2rem");
    button.style("border-radius", "50px");
    button.style("width", windowWidth-20 + "px");
    button.style("padding", "3rem");
    button.style("border", "15pt ridge pink");
    button.style("background-color", "green");
    button.style("color", "lightgreen");
    

    button.center();

    button.mousePressed(requestAccess);
  } else {
    // non IOS13 (android)
    others = true;
    // Way to do this in 2019 +
    window.addEventListener("deviceorientation", handleOrientation, true);
  }
}

let cx, cy;

function setup() {
  createCanvas(windowWidth, windowHeight);
  cx = width / 2;
  cy = height / 2;
  strokeWeight(10);
  window.addEventListener("deviceorientation", handleOrientation, true);
}
function draw() {
  if(ios13){
    // si la permission n'est pas autorisé, on retourne au debut de boucle
     if(!permissionGranted) return;
  }
  const dx = constrain(rotationY, -3, 3);
  const dy = constrain(rotationX, -3, 3);
  cx += dx * 2;
  cy += dy * 2;

  cx = constrain(cx, 0, width);
  cy = constrain(cy, 0, height);

  //ellipse(cx, cy, 10, 10);
  //line(rotationX * 4, rotationY * 4, pRotationX * 4, pRotationY * 4);
  line(rotationX * 6, rotationY * 6, pRotationX * 6, pRotationY * 6);

  fill("red");
}
